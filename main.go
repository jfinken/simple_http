package main

import (
	"flag"
	"log"
	"net/http"
	"os"
)

func main() {
	localPath := flag.String("local_path", "", "Path containing static files to be served.")
	flag.Parse()

	// require file path
	if *localPath == "" {
		flag.Usage()
		os.Exit(1)
	}
	fs := http.FileServer(http.Dir(*localPath))
	http.Handle("/", fs)

	log.Println("Listening on :3000...")
	err := http.ListenAndServe(":3000", nil)
	if err != nil {
		log.Fatal(err)
	}
}
